---
title: "SÉSAME R/RStudio"
subtitle: "Import et statistiques descriptives"
author: "GT Vallet"
output:
  learnr::tutorial:
    progressive: true
    allow_skip: true
    fig_caption: true
    language: fr
runtime: shiny_prerendered
description: "École d'été du SÉSAME, Université du Québec à Trois-Rivières (UQTR, Qc, CA)"
licence: "CC BY-NC 4.0"
---

```{r setup, include=FALSE}
library(learnr)
library(gradethis)
knitr::opts_chunk$set(echo = T, exercise.checker = gradethis::grade_learnr, message = F, warning = F)
```

<!-- #################################################### DATA IMPORT #################################################### -->
## Importation de données

Les analyses statistiques sont conduites sur des données et ces dernières sont le plus souvent générées/obtenues par d'autres moyens que R.
Il est alors nécessaire d'importer ces données dans R avant tout autre chose.

Le plus probable est que ces données soient disponibles sous forme d'un classeur de type Excel ou Calc. 
Toutefois, les bonnes pratiques en recherche et analyses statistiques recommandent d'utiliser des formats ouverts, transplateformes, accessibles et simples dans leur structure, typiquement des fichiers textes. 
Le format le plus facile à utiliser sera alors le format `CSV` ("Coma Separated Values") dans lequel la séparation entre les colonnes d'un classeur est symbolisée par une virgule. 
En raison de l'ambiguïté avec le symbole des décimales en français notamment, il existe des variantes au format `CSV` avec des points-virgules, des espaces ou des tabulations, etc.
Il faudra spécifier cette information lors de l'importation d'un fichier `CSV` dans R.

Vous pourriez rencontrer d'autres formats comme le `JSON` ou le `YAML`, mais leur présentation irait au-delà des objectifs de ce cours.
Il est toutefois très facile de trouver des compléments d'information sur Internet si jamais vous aviez à utiliser ces formats.

Les bonnes pratiques recommandent également d'avoir un fichier de données brutes, c'est-à-dire non transformées, qui doit demeurer tel quel.
Il est important de pouvoir revenir à vos données initiales malgré tous les traitements effectués si jamais ces traitements étaient à revoir ou si de nouvelles analyses, avec d'autres règles et contraintes, devaient être conduites. 
Ainsi, vous devriez toujours garder le fichier initial de donnée intacte et effectuer vos transformations, tris, filtres, et ainsi de suite sur les données importées dans R. Libre à vous alors d'enregistrer ces données transformées dans un nouveau fichier texte.

### Importer un tableur (fonction)
Voici la procédure pour importer un fichier de données au format Excel dans R.
Attention, afin que R puisse importer ces données, il faudra qu'elles soient organisées sans cellules fusionnées, lignes ou colonnes vides, etc. 
En outre, les formules existantes dans le fichier ne seront pas importées, seul leur résultat sera pris en compte.

Pour ce faire, le package `readxl` (installation `install.packages("readxl")`) pourra être utilisé (ou encore le package `xlsx`), voir par exemple la documentation [ici](http://www.sthda.com/english/wiki/reading-data-from-excel-files-xls-xlsx-into-r).

```{r eval=F}
# Appel du package readxl
library(readxl)
# Appel de la fonction readxl
mesData = read_excel('nomFichierExcel.xls(x)')
```

Il est possible de choisir le fichier à importer de manière interactive avec la fonction `file.choose()` :
```{r eval=F}
mesData = read_excel( file.choose() )
```

Il est également possible de spécifier la page du classeur à importer :
```{r eval=F}
# Importation de la page "data" du fichier monFichierExcel.xlsx
mesData = read_excel('nomFichierExcel.xlsx', sheet='data')

# Importation de la 2e page du fichier monFichierExcel.xlsx
mesData = read_excel('nomFichierExcel.xlsx', sheet=2)
```


### Importer un tableur (conversion)
L'import d'un tableur Excel ou Calc nécessite le recours à des packages supplémentaires.
Il est parfois plus simple de convertir directement les données Excel ou Calc au format CSV afin de s'assurer de la pérennité des données, de l'universalité du format, et plus simplement vérifier que les données seront bien importées comme souhaité dans R.

Il existe de très nombreux tutoriels sur comment enregistrer un fichier Excel ou Calc au format CSV (il suffit de passer par la fonction "enregistrer sous" et de choisir le format d'export), voici une [ressource vidéo](https://www.youtube.com/watch?v=W5H-lXittd4) (en anglais) et une [ressource textuelle](https://support.microsoft.com/fr-fr/office/enregistrer-un-classeur-au-format-texte-txt-ou-csv-3e9a9d6c-70da-4255-aa28-fcacf1f081e6) en français.

Une fois la conversion faite, vous pourrez suivre les instructions pour l'import d'un fichier CSV ci-dessous.


### Importer un fichier CSV
R propose une fonction native pour l'import de fichiers CSV, `read.csv`.
Il faudra alors spécifier le nom du fichier, si la première ligne contient ou non le nom des variables (le 'header'), le type de délimitation ('sep' qui peut être des virgules, points-virgules, etc.) :

```{r eval=F}
# Import du fichier CSV nomDeFichier.csv avec un header et des points-virgules comme séparateurs de colonnes
mesData = read.csv('nomDeFichier.csv', header = T, sep = ";")
```

Pour des fichiers plus complexes, il est possible de spécifier la ligne où doit débuter l'import, le nombre de lignes à lire, le nom des colonnes à donner, etc.

Les mêmes résultats pourront être obtenus avec l'usage de la fonction `read_csv()` ou `read_csv2()` du package `readr` introduit dans la partie précédente.
L'intérêt de l'usage de `read_csv` est multiple, cohérence du nom des paramètres (par ex. 'col_names' au lieu de 'header'), obtention d'un tibble au lieu d'un dataframe, objet proposé par le `tidyverse` pour être plus complet que le dataframe, affichage des caractéristiques des colonnes (mode, etc.).
La différence entre `read_csv()` et `read_csv2()` est que `read_csv()` importe des fichiers avec des virgules comme séparateur alors que `read_csv2()` se base sur des ";".

```{r eval=F}
# Appel du package
library(readr)
# Appel de la fonction 
mesData = read_csv('nomDeFichier.csv')
```


**Application**

```{r dtSetup, echo=F}
library(dplyr)
library(readxl)
library(readr)
library(ggpubr)
dtPsy = read_csv2('https://gitlab.com/gtvallet/ateliers-r/-/raw/master/src/dtPsy.csv?inline=false')
dtCovid = read_csv('https://gitlab.com/gtvallet/ateliers-r/-/raw/master/src/dtCovid.csv?inline=false')
```

Télécharger le fichier Excel à l'adresse suivante :  [https://gitlab.com/gtvallet/ateliers-r/-/raw/master/src/Data-AtelierR.xlsx](https://gitlab.com/gtvallet/ateliers-r/-/raw/master/src/Data-AtelierR.xlsx)

Déplacer le fichier dans votre répertoire de travail pour l'atelier.

1. Importer la 3e page du classeur en tant que fichier Excel dans une variable appelée "dtPsy".  

*Vous pouvez vérifier votre code ici dans la partie suivante (`Solution`)*


2. Importer la 2de page du classeur en tant que fichier CSV dans une variable appelée "dtCovid". (Garder le même nom de fichier)  

*Vous pouvez vérifier votre code dans la partie suivante (`Solution`)*


### Solution

1. `dtPsy = read_excel('Data-AtelierR.xlsx', sheet=3)`
2. `dtCovid = read_csv('dtCovid.csv')`



<!-- #################################################### STATS DESCRIPTIVES #################################################### -->
## Statistiques descriptives

### Vérification des données
Une fois l'importation des données réalisée, il est recommandé de vérifier l'allure du dataframe.
Pour ce faire, il est possible d'appeler la variable contenant le dataframe ou encore d'utiliser les fonctions `head()` (premières lignes) ou `tail()` (dernières lignes) de R.

```{r}
# Utilisation du jeu de données mtcars proposé par défaut 
mtcars
# Affichage des 6 premières lignes du dataframe 'mtcars'
head(mtcars)
# Affichage des 9 premières lignes du dataframe 'mtcars'
head(mtcars, n = 9)
# Affichage des 6 dernières lignes du dataframe 'mtcars'
tail(mtcars)
```

La structure du dataframe, précisant le type de variable et un aperçu du contenu peut être obtenu avec les fonctions `str()` de R et `glimpse()` du package `dplyr`.

```{r}
# Affichage de la structure du dataframe
str(mtcars)

# Appel du package dplyr
library(dplyr)
# Utilisation de la fonction glimpse (équivalent à str)
glimpse(mtcars)
```

### Fonctions de base
R propose de base l'ensemble des fonctions nécessaire pour déterminer les statistiques descriptives habituelles. 
Voici un tableau présentant quelques fonctions couramment employées :

| Nom      | Fonction         | Exemple             |
|----------|------------------|---------------------|
| length   | nombre/longueur  | `length(mtcars$hp)` |
| is.na    | donnée manquante | `is.na(mtcars$hp)`  |
| min      | minimum          | `min(mtcars$hp)`    |
| max      | maximum          | `max(mtcars$hp)`    |
| range    | étendue          | `range(mtcars$hp)`  |
| mean     | moyenne          | `mean(mtcars$hp)`   |
| sd       | écart-type       | `sd(mtcars$hp)`     |
| quantile | quartile/quantile| `quantile(mtcars$hp)`|
| var      | variance         | `var(mtcars$hp)`    |
| median   | médiane          | `median(mtcars$hp)` |

**Attention**, il est possible que la fonction retourne `NA` (*'not available'*) si le vecteur contient des données manquantes. 
Dans ce cas, il suffit de préciser qu'il faut omettre les données manquantes avec le paramètre `na.rm = TRUE` par exemple `mean(mtcars, na.rm = T)`.

**Application**

Calculer l'âge moyen des patients pour le jeu de données "dtPsy" 
```{r meanCovid, exercise=TRUE, exercise.setup="dtSetup"}
# Taper votre code ici 
```

```{r meanCovid-check}
grade_result(
  pass_if(~ identical(.result, 49.86), "Bien joué !"),
  fail_if(~ TRUE, "Vérifiez que vous avez bien spécifié un seul vecteur correspond à la colonne 'Age'.")
)
```

### Statistiques descriptives globales
Toutefois, l'usage de ces fonctions se limite à un vecteur, soit une variable, une ligne ou une colonne spécifique. 
Afin d'obtenir ces données descriptives pour l'ensemble du dataframe, il est possible de faire appel à la fonction `summary()` de R, ou encore à des packages (notamment `pastecs`, `hmsic` ou encore `summarytools`). 
Dans le cadre de cet atelier, ce sera la fonction `describe()` du package `psych` qui sera détaillée.

```{r}
# Données descriptives avec la fonction de base de R
summary(mtcars$hp)

# Données descriptives plus complètes avec 'describe()' du package 'psych'
library(psych)
describe(mtcars)
```

Cette fonction renvoie par variable (colonne du dataframe) la position de la variable (`var`), le nombre d'observations (`n`), la moyenne (`mean`), l'écart-type (`sd`), la médiane (`median`),  la moyenne ajustée/tronquée (`trimmed`),  médiane absolue (`mad`), minimum et maximum, l'asymétrie (`skew`), l'aplatissement (`kurtosis`) et l'erreur standard (`se`).

Ces fonctions sont très puissantes pour explorer un ensemble de données, mais il souvent pertinent de spécifier ces statistiques descriptives selon les modalités d'une ou plusieurs variables. 
Le package `psych` permet très facilement de déterminer ces valeurs par "groupes" (modalités) avec le paramètre `group` de la fonction `describeBy` :

```{r}
# Données descriptives par cylindres
describeBy(mtcars, group = "cyl")

# Données descriptives par type de transmission et par cylindres
describeBy(mtcars, group = c("vs", "cyl"))

# L'ordre d'entrée des variables change la présentation des données
describeBy(mtcars, group = c("cyl","vs"))
```

*A noter*, certaines statistiques pour certains groupes ne peuvent pas être déterminées (retour `NAN`), par exemple le paramètre `skew` pour les 8 cylindrées, en raison de l'absence de variance des données (`sd = 0`). Il en irait de même pour le calcul d'une corrélation avec des données qui ne varient pas.

**Application**

1. Utiliser la fonction `describe()` pour obtenir les statistiques descriptives des données "dtCovid"  

*Vous pouvez vérifier votre code ici :*

```{r describeCovid, exercise=TRUE, exercise.setup="dtSetup"}
# Taper votre code ici
```

```{r describeCovid-solution}
describe(dtCovid)
```

```{r describeCovid-check}
grade_code("Très bien !")
```

2. Utiliser la fonction `describeBy()` pour obtenir les statistiques descriptives des données "dtPsy" par type de diagnostic.  

*Vous pouvez vérifier votre code ici :*

```{r describePsy, exercise=TRUE, exercise.setup="dtSetup"}
# Taper votre code ici
```

```{r describePsy-solution}
describeBy(dtPsy, group = "Diag")
```

```{r describePsy-check}
grade_code("Très bien !")
```


### Méthode semi-manuelle
Les fonctions présentées ci-dessus sont particulièrement pertinentes afin d'explorer les données.
Toutefois, il est souvent utile de produire des données descriptives plus spécifiques pour un sous-ensemble de variables. 
Dans ce cas de figure, la solution la plus puissante est de combiner des fonctions de bases de R avec la manipulation des données de `dplyr`.
Outre l'introduction à la grammaire du `tidyverse`, l'usage plus complet de `dplyr` ne sera pas détaillé dans ce document ([mais voir ici](https://larmarange.github.io/analyse-R/manipuler-les-donnees-avec-dplyr.html)).

Les packages du `tidyverse` utilisent un connecteur `%>%` pour "chainer" les opérations (pipeline en anglais).
Ce connecteur peut être inséré dans RStudio en utilisant le raccourci clavier `ctl-shift-m` (ou `cmd-shift-m` sous MacOS).
Ainsi, il est possible de combiner un grand nombre de manipulations/fonctions à partir d'un jeu de données initial.
Voici un exemple simple pour calculer la moyenne et l'écart-type de la consommation moyenne.

```{r}
# Appel du package dplyr
library(dplyr)
# A partir du jeu de données mtcars, 
#    création de nouvelles variables "Moy" et "ET"
#    calculées avec les fonctions mean() et sd()
mtcars %>% summarise(Moy = mean(mpg), ET = sd(mpg))

# Dplyr permet d'utiliser directement le nom des colonnes comme paramètre pour les fonctions
```

Voici un exemple un peu plus complexe pour calculer la moyenne et l'écart-type de la consommation moyenne par type de cylindrés et type de transmission.

```{r}
mtcars %>%                   # sélection du dataframe
  group_by(cyl, am) %>%      # groupement des données par cylindrés et par transmission
  summarise(Moy = mean(mpg), # calcul de la moyenne et
            ET = sd(mpg))   # calcul de l'écart-type de la consommation moyenne
```

Enfin, voici un exemple encore un peu plus complexe pour calculer la moyenne et l'écart-type de la consommation moyenne et de l'accélération par type de cylindrés et type de transmission en arrondissant à deux décimales.

```{r}
mtcars %>%                                 # sélection du dataframe
  group_by(cyl,am) %>%                     # groupement des données par cylindrés et par transmission
  summarise(MoyConso = mean(mpg), ETconso = sd(mpg),   # calcul de la moyenne et de l'écart-type de la consommation moyenne
            MoyAcc = mean(qsec), ETAcc = sd(qsec)) %>% # calcul de la moyenne et de l'écart-type de l'accélération
  mutate(across(where(is.numeric), round, 2))          # arrondit l'ensemble des résultats s'ils sont des nombres
```

**Application**

1. Créer un tableau regroupant le minimum et le maximum du nombre de cas au 7 mars ("NbCas7mars", avec "minNbCas" et "maxNbCas") et l'augmentation du nombre de cas sur 4 jours ("Aug4j" avec "minAug" et "maxAug") des données "dtCovid".

*Vous pouvez vérifier votre code ici :*

```{r dplyrCovid, exercise=TRUE, exercise.setup="dtSetup"}
# Taper votre code ici
```

```{r dplyrCovid-solution}
dtCovid %>% summarise(minNbCas = min(NbCas7mars), maxNbCas = max(NbCas7mars),
                      minAug = min(NbCas7mars), maxAug = max(NbCas7mars))
```

```{r dplyrCovid-check}
grade_code("Très bien !")
```


2. Créer un tableau regroupant la moyenne ("Moy") et l'écart-type ("ET") obtenus pour l'intensité des symptômes dépressifs après la thérapie ("Sympt.Post.Dep") selon le diagnostic et le sexe des patients pour les données "dtPsy".

*Vous pouvez vérifier votre code ici :*

```{r dplyrPsy, exercise=TRUE, exercise.setup="dtSetup"}
# Taper votre code ici
```

```{r dplyrPsy-solution}
dtPsy %>% 
  group_by(Diag, Sexe) %>%
  summarise(Moy = mean(Sympt.Post.Dep), 
            ET  = sd(Sympt.Post.Dep))
```

```{r dplyrPsy-check}
grade_code("Très bien !")
```

3. [Usage avancé] Réaliser les calculs décrits pour la question 1 mais à l'aide de boucles `for` (voir chapitre précédent) et non grâce à `dplyr`. 


## Groupements et tableaux de contingence

### Tableaux
Les fonctions présentées ci-dessus donnent des résultats assez complets concernant les données à traiter.
Il est parfois simplement nécessaire de dénombrer les observations par modalités d'une ou plusieurs variables.
Ce type de groupement des données est aisément obtenu avec la fonction `table()` :
```{r}
# Groupement des observations par cylindrés
table(mtcars$cyl) # Noter que table travaille sur un vecteur et non un dataframe entier

# Groupement des observations par cylindrés et type de transmission
table(mtcars$cyl, mtcars$vs)

# Le même résultat peut être obtenu avec la fonction xtabs
xtabs(~ cyl + vs, data = mtcars)
```

Bien entendu, il est possible d'ajouter les sommes marginales avec la fonction `margin.table()`. *Attention*, cette fonction doit s'appliquer sur un tableau déjà existant.
Toutefois, le plus simple est probablement d'appliquer la fonction `addmargins()` qui peut être chainée à la création du tableau :
```{r}
table(mtcars$cyl, mtcars$vs) %>% addmargins()
```

L'obtention de ce type de groupement de données peut également se faire sous la forme de fréquences avec la fonction `prop.table()`. 
*Attention*, cette fonction doit s'appliquer sur un tableau déjà créé (soit par l'appel d'une variable, soit par l'appel de la fonction `table()` en sein de la fonction `prop.table()`).

```{r}
# Groupement des observations par cylindrés et type de transmission
prop.table( table(mtcars$cyl, mtcars$vs) ) 
```

**Application**

Créer un tableau de contingence rapportant les fréquences d'hommes et de femmes selon le diagnostic pour les données "dtPsy".

*Vous pouvez vérifier votre code ici :*

```{r tablePsy, exercise=TRUE, exercise.setup="dtSetup"}
# Taper votre code ici
```

```{r tablePsy-solution}
prop.table( table(dtPsy$Diag, dtPsy$Sexe) ) 
```

```{r tablePsy-check}
grade_code("Très bien !")
```


### Formatage d'un tableau groupé
Il est possible de formater la sortie de ces fonctions afin de pouvoir les exploiter plus facilement (export des données, analyses spécifiques, etc.) en transformant le tableau en dataframe avec la fonction `as.data.frame()` :
```{r}
# Définition d'une variable 'tab' contenant le tableau de contingence 
tab = prop.table( table(mtcars$cyl, mtcars$vs, dnn = c("Cylindre", "Transmission")) )
# Formatage du tableau en un dataframe
as.data.frame( tab )
```

Enfin, il faudra utiliser la fonction `xtabs()` ou encore `ftable()` lorsqu'un groupement de données doit être effectué pour plus que deux variables :
```{r}
# Groupement des observations par cylindrés, type de transmission et par puissance
xtabs( ~cyl + vs + gear, data = mtcars  )

# Même tableau "à plat" avec ftable
ftable( cyl + vs ~ gear, data = mtcars  ) # Noter que la variable après le ~ sera en ligne
```

### Test de Chi-2
Les tableaux de contingences permettent de comparer rapidement et facilement les différentes proportions d'observations selon les modalités (indépendance des variables).
Le test le plus couramment employé pour tester cette équivalence entre les observations est le test du chi-2 ($\chi^2$).

Une fois le tableau de contingence créé avec `table()` ou `xtabs()`, il suffit d'appeler la fonction `summary()` sur le résultat pour obtenir les valeurs du test de chi-2.
```{r}
# Définition d'une variable 'propTab' contenant le tableau de contingence
propTab = xtabs( ~cyl + vs + gear, data = mtcars  )

# Réalisation du test de chi-2
summary(propTab)
```

Le même résultat peut être obtenu en appelant la fonction `chisq.test()` sur un tableau de contingence, mais uniquement pour des tableaux à deux dimensions (deux variables) :
```{r}
# Définition d'une variable 'propTab2' contenant le tableau de contingence à deux variables
propTab2 = xtabs( ~cyl + vs, data = mtcars  )

# Réalisation du test de chi-2
chisq.test(propTab2)
```

Une fois le test réalisé, vous pouvez accéder de manière isolée et spécifique aux différents paramètres du test (voir par ex. [ce site](http://www.sthda.com/french/wiki/test-de-chi2-avec-r)).
```{r}
monTest = chisq.test(propTab2)

monTest$statistic  # la statistique du Chi2.
monTest$parameter  # le nombre de degrés de liberté.
monTest$p.value    # la p-value.
monTest$observed   # la matrice observée de départ.
monTest$expected   # la matrice attendue sous l'hypothèse nulle d'absence de biais.
```

Lorsque les **données sont pairées** (devis intra-participant), il faut alors réaliser le test de McNemar avec la fonction `mcnemar.test()`. 
La procédure est similaire, le test prend en paramètre un tableau de contingence (à deux variables). 

**Remarque**, pour le test du chi-2 et de McNemar, il est possible d'ajouter un paramètre `correct = TRUE` afin de corriger le test lorsque les conditions d'application ne sont pas remplies.
L'ensemble de ces fonctions et des conditions d'applications sont notamment décrites [sur ce site](https://statistique-et-logiciel-r.com/comparaison-de-deux-pourcentages-avec-le-logiciel-r/).


<!-- #################################################### REP GRAPHIQUES #################################################### -->
## Représentations graphiques
R offre un puissant système pour créer et personnaliser tout type de représentation de données.
Toutefois, le manque de certaines fonctionnalités ou la complexité rencontrée pour certaines options a conduit au développement de plusieurs packages spécialisés dont les plus connus sont `lattice` et `ggplot`. 
Les fonctions présentées par la suite s'appuieront uniquement sur le package `ggplot2` par souci de cohérence avec le reste des packages du `tidyverse`.
Le package `ggpubr` propose une version simplifiée des fonctions de `ggplot` et sera donc également présenté.

Voici quelques liens pour découvrir ou approfondir `ggplot` :

- [documentation assez complète (en anglais)](http://r-statistics.co/Complete-Ggplot2-Tutorial-Part1-With-R-Code.html)
- [introduction pas-à-pas](https://statistique-et-logiciel-r.com/introduction-a-la-visualisation-sous-r-avec-le-package-ggplot2/)
- [guide générique](http://www.sthda.com/french/wiki/ggplot2-graphique-lineaire-guide-de-demarrage-rapide-logiciel-r-et-visualisation-de-donnees)


### Boxplot ou boites à moustaches
Les boites à moustaches permettent de représenter la distribution des observations pour une variable en rendant visible la médiane (ou parfois la moyenne), ainsi que l'intervalle interquartile. 
Cette forme de représentation est donc particulièrement adaptée pour repérer des données extrêmes (*outliers*).

Voici le code pour créer un boxplot avec le package `ggplot2` :
```{r}
# Appel du package ggplot2
library(ggplot2)
# Représentation de la consommation d'essence
ggplot(mtcars, aes(y = mpg)) +   # Définition des données à utiliser 
  geom_boxplot()                 # Affichage des données sous la forme d'un boxplot
```

*Notes* : `ggplot` s'utilise comme `dplyr` par chainage des opérations, mais cette fois par un `+` comme connecteur au lieu du `%>%`. 
La fonction `ggplot()` permet de sélectionner le dataframe et spécifier les variables (colonnes) à utiliser pour les axes (x et y) et éventuellement celles pour définir le type de forme, les couleurs, les groupes, etc. 
La fonction `aes()` permet d'utiliser directement le nom de la variable au lieu de redonner l'ensemble de l'indiçage, ici `mpg` au lieu de `mtcars$mpg`. 
Enfin, les fonctions `geom_XXX` permettent de spécifier le type de représentation graphique souhaité, ici un boxplot.
Il est possible d'ajouter des "couches" (*layers*) afin de représenter en même temps plusieurs types de représentation comme des lignes et des points.

Le package `ggpubr` permet de simplifier l'usage de `ggplot` en appelant directement le type de représentation et les variables par leurs noms sans passer par `aes()`(mais en ajoutant des guillemets) :
```{r}
# Appel du package ggpubr
library(ggpubr)
# Représentation de la consommation d'essence
ggboxplot(mtcars, y = "mpg")
```

Là encore, il est facile de représenter les différentes modalités d'une même variable en spécifiant l'axe des "x" et éventuellement le paramètre `group`. 
Il est alors possible de personnaliser l'affichage pour utiliser différentes formes ou couleurs, notamment des données extrêmes.

```{r}
# Représentation de la consommation d'essence par cylindrés
ggplot(mtcars, aes(x = cyl, y = mpg, group = cyl)) + 
  geom_boxplot()

# Représentation de la consommation d'essence par cylindrés avec les outliers en couleur
ggplot(mtcars, aes(x = cyl, y = mpg, group = cyl)) + 
  geom_boxplot(outlier.colour = "red")

# Même graphique avec ggpubr et une forme différente pour les outliers 
ggboxplot(mtcars, y = "mpg", x = "cyl", outlier.shape = 4)
```

**Application**

Créer un boxplot des symptômes anxieux après la thérapie en fonction du diagnostic à l'aide de la fonction `ggboxplot()`

*Vous pouvez vérifier votre code ici :*

```{r boxplot, exercise=TRUE, exercise.setup="dtSetup"}
# Taper votre code ici
```

```{r boxplot-solution}
ggboxplot(dtPsy, y = "Sympt.Post.Anx", x = "Diag")
```

```{r boxplot-check}
grade_code("Très bien !")
```


### Histogrammes
Les histogrammes permettent de représenter la distribution des observations d'une variable.
Cette représentation graphique est recommandée pour explorer visuellement la normalité des données (pré-requis pour une majorité d'analyses paramétriques).
Les données extrêmes peuvent également être repérées, même si un boxplot semble plus approprié pour rechercher cette information.
```{r}
# Affichage de la distribution de la consommation d'essence
ggplot(mtcars, aes(x = mpg)) + 
  geom_histogram() +
  xlab("Miles par gallon") +    # Modification du titre de l'axe des abscisses
  ylab("Nombre d'observations") # Modification du titre de l'axe des ordonnées

# Même graphique avec ggpubr
gghistogram(mtcars, x = "mpg", xlab = "Miles par gallon", ylab = "Nombre d'observations")
```

La fonction `geom_histogram` permet de spécifier un groupement possible des observations, par exemple par tranche de 5 ici :
```{r}
ggplot(mtcars, aes(x = mpg)) +
   geom_histogram(binwidth=5)
```

Il est possible d'ajouter la densité avec une courbe (ggplot) ou par le nombre d'observations au-dessus de l'axe X ainsi que de représenter aisément la moyenne :
```{r}
ggplot(mtcars, aes(x = mpg)) +
   geom_histogram(aes(y=..density..), binwidth=5) +  # Code pour représenter une densité à la place d'un décompte
   geom_vline( aes(xintercept = mean(mpg) ))         # Code pour représenter la moyenne sur le graphique

ggdensity(mtcars, x = "mpg", 
              rug = TRUE,      # rug permet d'ajouter les "traits"/observations en bas du graphique
              add = "mean")    # add précise qu'il faut ajouter la moyenne au graphique
```

Ou encore un histogramme de densité selon une combinaison de toutes ces informations avec `ggpubr` :
```{r}
gghistogram(
  mtcars, x = "mpg", y = "..density..",
  add = "mean", rug = TRUE,
  add_density = TRUE
  )
```


Là encore, plusieurs modalités d'une variable ou plusieurs variables peuvent être représentées conjointement :
```{r}
# Spécification de la variable "vs" comme facteur
mtcars$vs = factor(mtcars$vs)

# Représentation des histogrammes par type de transmission
ggplot(mtcars, aes(x = mpg, fill = vs)) +
   geom_histogram(aes(fill=vs), binwidth=4) 
```

Avec `ggpubr` :
```{r}
# Spécification de la variable "cyl" comme facteur
mtcars$cyl = factor(mtcars$cyl)

# Représentation des densités selon les cylindrés
ggdensity(mtcars, x="mpg", fill="cyl")
```


### Autres graphiques
D'autres représentations graphiques peuvent être requises pour des analyses spécifiques comme les effectifs cumulés ou encore la représentation de la normalité avec un graphique Q-Q.

Voici le code `ggpubr` pour les effectifs cumulés :
```{r}
ggecdf(mtcars, x = "mpg")
```

Et le code pour un Q-Q plot :
```{r}
ggqqplot(mtcars, x = "mpg")
```

Les autres représentations graphiques peuvent être obtenues suivant un principe similaire grâce à `ggplot2` ou `ggpubr`.


<!-- #################################################### EXERCICES #################################################### -->
## Exercices 

### Indiçage et sous-ensembles
A partir des données "dtPsy" :

1. Créer un sous-ensemble de données contenant uniquement les données des hommes.
2. De cet ensemble sélectionner la 3e et 5e ligne.
3. Toujours à partir de cet ensemble, sélectionner uniquement les colonnes "Patient", "Diag", "Symt-Pre-Anx" et "Symt-Pre-Dep".
4. A partir de l'ensemble des données, sélectionner uniquement les patients ayant des scores pour leurs symptômes pré thérapie supérieurs à 20 pour l'anxiété et supérieurs à 18 pour la dépression.
5. A partir de l'ensemble des données, sélectionner uniquement les patients étant soit des femmes soit ayant 40 ans ou moins.

```{r eval=F, echo=F}
# Uniquement des hommes
ho = dtPsy %>% filter(Sexe=="M")
# 3 et 5e lignes des hommes
ho[c(1,5), ]
# Colonnes "Patient", "Diag", "Symt-Pre-Anx" et "Symt-Pre-Dep"
ho %>% select(Patient, Diag, Sympt.Pre.Anx, Sympt.Pre.Dep)
# Symptômes pré thérapie > 20 anxiété et > 18 dépression
dtPsy %>% filter(Sympt.Pre.Anx>20 & Sympt.Pre.Dep > 18)
# Femme ou <= 40 ans
dtPsy %>%  filter(Sexe=="F" | Age <= 40)
```


### Statistiques descriptives
A partir des données "dtPsy" :

1. Déterminer la structure du jeu de données avec la fonction `glimpse()`.
2. Obtenir les statistiques descriptives du jeu de données en fonction du diagnostic et du sexe des patients.  
3a. Représenter par des boxplot l'anxiété pré thérapie des patients selon leur sexe.  
3b. Représenter par des histogrammes de densité l'âge des patients selon leur diagnostic.
4. Créer un tableau rapportant la médiane pré/post pour les symptômes dépressifs selon le diagnostic et le sexe des patients.
5. Créer un tableau de contingence croisant le diagnostic et le sexe des patients. 
6. Déterminer si proportion homme/femme est similaire selon le diagnostic.

```{r eval=F, echo=F}
# Glimpse
glimpse(dtPsy)
# Stat descr diag~sexe
describeBy(dtPsy, group=c("Sexe", "Diag"))
# Boxplot anxiété pré thérapie ~ sexe
ggboxplot(dtPsy, y="Sympt.Pre.Anx", x="Sexe")
# Histogramme âge~diagnostic
ggdensity(dtPsy, x="Age", fill="Diag", color="Diag", rug=T)
# Médiane pre/post dépression selon diag et sexe
dtPsy %>% 
  group_by(Diag, Sexe) %>% 
  summarise(medPreDep = median(Sympt.Pre.Dep),
            medPostDep = median(Sympt.Post.Dep) 
)
# Tableau de contingence diag~sexe
table(dtPsy$Diag, dtPsy$Sexe)
# Chi2
summary(table(dtPsy$Sexe,dtPsy$Diag))
```

La représentation graphique des données d'un tableau de contingence peut s'effectue à l'aide du package `plotxtabs` (installation : `install.packages("plotxtabs")`). 
Une présentation du package est disponible [ici](https://cran.r-project.org/web/packages/CGPfunctions/vignettes/Using-PlotXTabs.html).
