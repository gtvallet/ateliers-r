# Initiation au logiciel R

**GT Vallet** -- Mai 2023

*École d'été du SÉSAME -- Université du Québec à Trois-Rivières (UQTR)*

## Présentation
Ateliers de formation à R/RStudio d'une durée de 12h visant à introduire la réalisation d'analyses statistiques avec R.

L’objectif de ce module est d’initier les doctorant-e-s au langage R afin de réaliser des analyses statistiques. 
R est un logiciel d'analyses statistiques, comme SPSS ou Statistica, qui est aussi un langage de programmation (comme C ou Python). 
Cette particularité fait de R un outil très puissant pour automatiser et réaliser des traitements statistiques et les mettre en forme.

C'est un outil libre, gratuit et multiplateforme ce qui le rend universel et conforme aux principes universitaire. 

La formation permettra d'aborder notamment les points suivants :  syntaxe de R, installation/utilisation des bibliothèques ("packages"), statistiques descriptives et les représentations graphiques, quelques tests inférentiels. 

## Pré-requis
- connaitre les statistiques descriptives (échelles de mesure, paramètres de position et de dispersion, représentations graphiques...),
- connaitre a minima les principes sous-jacents aux statistiques inférentielles (test d'hypothèses, valeur de p...) et de quelques tests inférentiels classiques (par ex. chi-2, t de Student, ANOVA),
- disposer pour le module d'un ordinateur portable avec R et RStudio installés