École d'été du SÉSAME sur R/RStudio (UQTR)
========================================================
author: Guillaume Vallet
date: 23/05/2023
autosize: true


Qui suis-je ?
========================================================
incremental: true

- Directeur du SÉSAME
- Professeur suppléant en neuropsychologie (spé psy cognitive et neuropsychologie)
- (Neuro)Psychologue, pas mathématicien ou statisticien ou informaticien
- Formation "classique" en statistiques
- Formation à SPSS
- **Autoformation** à Matlab, Python et R


Qui êtes-vous ?
========================================================

![](src/question.jpg)


Pour mieux cibler la formation
========================================================

**https://ahaslides.com/FP3H3**

![](src/sondage.png)


Avertissements
========================================================
incremental: true

- *"steep learning curve"*
- Persévérance !
- Patience !


Quelques conseils
========================================================
incremental: true

- Savoir chercher de l'aide / S'entraider
- Apprendre à apprendre vs. **résolution de problèmes**
- Se forcer à utiliser R
- Commencer petit, avancer un pas à la fois
- Adapter ce qui existe 
- Garder une trace


Démonstrations
========================================================

![](src/demo.jpg)


Programme
========================================================
incremental: true

1. Introduction à R/RStudio
2. Statistiques descriptives
3. Liens entre des variables (corrélation/régression)
4. Différence entre des variables (test *t*, ANOVA...)


Fonctionnement
========================================================
incremental: true

- Semi-guidé
- Tutoriels interactifs
- Ateliers --> mise en pratique


Ateliers
========================================================

https://gitlab.com/gtvallet/ateliers-r


Ateliers (2)
========================================================

[<img src="src/AtelierDownload.png" width="1200"/>](src/AtelierDownload.png)


Ateliers (3)
========================================================

Installation des packages nécessaires :

1. `install.packages(c("learnr", "remotes", "readxl", "tidyverse", "psych", "ggpubr", "GGally","correlation","corrgram", "corrplot", "olsrr", "afex", "emmeans"))`

2. `remotes::install_github("rstudio/gradethis")`


On commence ?
========================================================

[<img src="src/question.jpg" width="900"/>](src/question.jpg)

Des question ?


Iconographie
========================================================

- Freepik
- slidesgo / Freepik
- makyzz / Freepik

